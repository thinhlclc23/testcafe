Feature: test celebral website

    Check website functionality from create acount

    Scenario:Create an account Then check the function
        Given that I am a client who is seeking for an online anxiety and alcohol cessation service that is available in my state
        Given I found a link that directs me to Cerebral website
        Given I navigate to login page from the landing page
        When I click on I dont have an account
        And complete account information step
        And completed personal information step
        And select “Depression” and "Addiction-Alcohol" service line
        And answer a list of questions regarding to Depression and Alcohol
        Then I see Preliminary Screen Indicators screen showing my “Depression“ and “Alcohol” level
        When I click on “Continue”
        Then I see a "Med + Care Counseling" plan
        When I click on "VIEW MORE PLAN"
        Then I see Med + Care Counseling , Med + therapy, Therapy plan
        When I click on “Start Therapy ” on Therapy plan
        Then I get navigated to Billing Information screen
        And I see “Therapy”
        And the price is for first month is $23/week
        And the price for month later is $60/week
        When I complete check out
        And click “Start Meds + Therapy Plan” on add Medication plan
        And complete shipping information
        And complete post checkout question
        And Choose schedule with Prescriber
        And schedule later with Therapist
        And continue to the client portal
        Then I get navigated to Message box chat
        And see only greeting message from Cerebral Care team
        When I click on “Account”
        Then I cant see “Edit” on “PHARMACY BENEFITS / MEDICATION BILLING”
        And cant see “Check my insurance coverage” on INSURANCE INFORMATION
        When I click on "Appointments & Tasks"
        And Click Start on MY OUTSTANDING TASKS
        Then I can't see insurance popup
        When I open my email that is used to register my Cerebral account
        Then I receive a Welcome email from Cerebral
        And appointment confirmation email

