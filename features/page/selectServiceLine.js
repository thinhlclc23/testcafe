const { Selector } = require('testcafe');
const { Given, When, Then } = require('cucumber');
const { xPathSelector } = require('../common/getElementsByXPath');

class selectServiceLine {
    constructor() {
        this.depression = xPathSelector("//div[contains(text(),'Depression')]");
        this.substanceUse = xPathSelector("//div[contains(text(),'Substance Use')]");
        this.alcohol = xPathSelector("//div[contains(text(),'Alcohol')]");
        this.confirmBtn = Selector("button[class*='SubmitButton-sc']");
    }
}
module.exports = new selectServiceLine();