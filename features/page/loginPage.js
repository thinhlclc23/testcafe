const { Selector } = require('testcafe');
const { Given, When, Then } = require('cucumber');
const { xPathSelector } = require('../common/getElementsByXPath');

class LoginPage {

    constructor() {

        this.emailTextBox = Selector('#input-email');
        this.passwordTextBox = Selector('#input-password');
        this.accountLink = Selector('input').withAttribute("value", "I don't have an account.");

    }



}
module.exports = new LoginPage();