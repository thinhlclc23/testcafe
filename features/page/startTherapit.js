const { Selector } = require('testcafe');
const { Given, When, Then } = require('cucumber');
const { xPathSelector } = require('../common/getElementsByXPath');

class startTherapit {

    constructor() {

        this.startMedAndTherapiTodayBtn = Selector("div[class*=styled__SubPrice-sc] + button");
        this.confirmBtn = xPathSelector("//button[normalize-space()='Confirm']");
        this.continuePlanBtn = Selector("button[class*=TextButton-sc]");

    }


}
module.exports = new startTherapit();