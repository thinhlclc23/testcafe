const { Selector } = require('testcafe');
const { Given, When, Then } = require('cucumber');
const { xPathSelector } = require('../common/getElementsByXPath');

class checkOutInformation {

    constructor() {
        this.inputNameCredit = Selector('input[placeholder*=\'Name on card\']');
        this.inputNumberCredit = Selector('input[placeholder*=\'Credit Card Number\']');
        this.inputMMYYCredit = Selector('input[placeholder*=\'MM \/ YY\']');
        this.inputCVCCredit = Selector('input[placeholder*=\'CVC\']');
        this.inputZIPCredit = Selector('input[placeholder*=\'ZIP\']');
        this.iframe1 = Selector("iframe").nth(0);
        this.iframe2 = Selector("iframe").nth(1);
        this.iframe3 = Selector("iframe").nth(2);
        this.submitBtn = Selector("button[class*='SubmitButton-sc']");
    }
}
module.exports = new checkOutInformation();