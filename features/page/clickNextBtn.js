const { Selector } = require('testcafe');
const { Given, When, Then } = require('cucumber');
const { xPathSelector } = require('../common/getElementsByXPath');

class clickNextBtn {

    constructor() {
        this.nextBtn = Selector("button[class*=NextButton-sc]");
        this.firstChoiceBtn = Selector('button[class*=ButtonSelector-sc]:first-of-type');
        this.submitBtn = Selector("button[class*='SubmitButton-sc']");
        this.firstOption = xPathSelector("//span[normalize-space()='Loss of a loved one']");
        this.viewMorePlansLinkBtn = Selector("div[class*=styled__ViewMorePlans-sc]");
        this.startTherapiBtn = xPathSelector("//button[contains(text(),'Start Therapy Today')]");
    }


}
module.exports = new clickNextBtn();