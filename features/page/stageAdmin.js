const { Selector } = require('testcafe');
const { xPathSelector } = require('../common/getElementsByXPath');

class stageAdmin {
    constructor() {
        this.emailLogin = Selector("input#associate_email");
        this.passwordLogin = Selector("input#associate_password");
        this.loginBtn = Selector("input[name=commit]");
        //
        this.searchNameTxt = Selector("input[name=patient_name]");
        this.emailChecker = xPathSelector("//td[contains(text(),'mubaohiem48@gmail.com')]//following-sibling::td/a");
        //
        this.vertifySuccess = xPathSelector("//button[normalize-space()='Verification Complete']");


    }
}

module.exports = new stageAdmin();