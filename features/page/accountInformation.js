const { Selector } = require('testcafe');
const { Given, When, Then } = require('cucumber');
const { xPathSelector } = require('../common/getElementsByXPath');

class accountInformation {

    constructor() {

        this.emailTextBox = Selector('#input-email');
        this.phoneNumberTextBox = Selector('input').withAttribute('class', 'sc-htpNat fMiARp');
        this.createPasswordTextBox = Selector('#input-password');
        this.confirmPasswordTextBox = Selector('#input-password_confirm');
        this.confirmCheckbox = Selector('span').withAttribute('class', 'checkbox-checkmark');
        this.getStartedButton = Selector('button').withAttribute('class', 'SubmitButton-sc-1sdhfzr-0 JOKWQ');

    }


}
module.exports = new accountInformation();