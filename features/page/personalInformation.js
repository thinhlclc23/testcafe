const { Selector } = require('testcafe');
const { Given, When, Then } = require('cucumber');
const { xPathSelector } = require('../common/getElementsByXPath');

class personalInformation {

    constructor() {

        this.firstNameTextBox = Selector('#input-given-name');
        this.lastNameTextBox = Selector('#input-family-name');
        this.zipCodeTextBox = Selector('#input-postal-code');
        this.dateOfBirthTextBox = Selector('#input-dob');
        this.continueButton = Selector('button').withAttribute('class', 'SubmitButton-sc-1sdhfzr-0 JOKWQ');
        // this.commonCheckbox = Selector('span').withAttribute(' class', value);


    }


}
module.exports = new personalInformation();