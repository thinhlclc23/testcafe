const { Selector } = require('testcafe');
const { Given, When, Then } = require('cucumber');
const { xPathSelector } = require('../common/getElementsByXPath');

class shippingInformation {

    constructor() {
        this.addressLine1Txt = Selector("input#input-address-line1");
        this.addressLine2Txt = Selector("input#input-address-line2");
        this.cityTxt = Selector("input#input-city");
        this.stateChoose = Selector("select#input-region");
        this.zipCodeShippingTxt = Selector("input#input-postal-code");

    }


}
module.exports = new shippingInformation();