const { Selector } = require('testcafe');
const { Given, When, Then } = require('cucumber');
const login = require('../page/loginPage');
const actionCommon = require('../common/actioncommon');
const accountInformation = require('../page/accountInformation');
const personalInformation = require('../page/personalInformation');
const selectSeviceLine = require('../page/selectServiceLine');
const clickNextBtn = require('../page/clickNextBtn');
const checkOutInformation = require('../page/checkOutInformation');
const startTherapit = require('../page/startTherapit');
const shippingInfor = require('../page/shippingInfor');
// const actioncommon = require('../common/actioncommon');




const userClient = 'ThinhVu' + actionCommon.generateRandomString(5) + '@gmail.com';
const passwordClient = '12345678aB@';



// When('And select Anxiety service line', async function() {
//     await actionCommon.clickCheckBox(this.commonCheckbox('d'));
//     await testController.wait(10000);
// });


// Given('Open Celebral website {string}', async function (url) {
//     await login.navigateToUrl(url);
// });

// When('User login to the website', async function () {
//     await login.LoginPageByUser();
// });
Given('that I am a client who is seeking for an online anxiety and alcohol cessation service that is available in my state', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Given('I found a link that directs me to Cerebral website', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Given('I navigate to login page from the landing page', async function() {
    await actionCommon.navigateToUrl("https://app-fe-release.getcerebral.com/");
});



When('I click on I dont have an account', async function() {
    await testController.wait(7000);
    await actionCommon.clickElement(login.accountLink);
});

When('complete account information step', async function() {
    await actionCommon.typeTextToTextBox(accountInformation.emailTextBox, userClient);
    console.log('acc la: ' + userClient);
    await actionCommon.typeTextToTextBox(accountInformation.phoneNumberTextBox, '0123456789');
    await actionCommon.typeTextToTextBox(accountInformation.createPasswordTextBox, passwordClient);
    await actionCommon.typeTextToTextBox(accountInformation.confirmPasswordTextBox, passwordClient);
    await actionCommon.clickElement(accountInformation.confirmCheckbox);
    await actionCommon.clickElement(accountInformation.getStartedButton);
});

When('completed personal information step', async function() {
    await actionCommon.typeTextToTextBox(personalInformation.firstNameTextBox, 'Thinh');
    await actionCommon.typeTextToTextBox(personalInformation.lastNameTextBox, 'Vu');
    await actionCommon.typeTextToTextBox(personalInformation.zipCodeTextBox, '77089');
    await actionCommon.typeTextToTextBox(personalInformation.dateOfBirthTextBox, '03301999');
    await actionCommon.clickElement(personalInformation.continueButton);
});



When('select “Depression” and "Addiction-Alcohol" service line', async function() {
    // Write code here that turns the phrase above into concrete actions
    await actionCommon.clickCheckBox(selectSeviceLine.depression);
    await actionCommon.clickCheckBox(selectSeviceLine.substanceUse);
    await actionCommon.clickElement(selectSeviceLine.confirmBtn);
    await actionCommon.clickCheckBox(selectSeviceLine.alcohol);
    await actionCommon.clickElement(selectSeviceLine.confirmBtn);
});



When('answer a list of questions regarding to Depression and Alcohol', async function() {
    // Write code here that turns the phrase above into concrete actions
    await actionCommon.clickElement(clickNextBtn.nextBtn);
    await actionCommon.clickElement(clickNextBtn.nextBtn);
    await actionCommon.clickElement(clickNextBtn.nextBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.submitBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstChoiceBtn);
    await actionCommon.clickElement(clickNextBtn.firstOption);
    await actionCommon.clickElement(clickNextBtn.submitBtn);
});



Then('I see Preliminary Screen Indicators screen showing my “Depression“ and “Alcohol” level', async function() {
    // assert Indicators


});



When('I click on “Continue”', async function() {
    // Write code here that turns the phrase above into concrete actions
    await actionCommon.clickElement(clickNextBtn.submitBtn);
});



Then('I see a \"Med + Care Counseling\" plan', async function() {
    // assert Mecdication and Counseling
});



When('I click on \"VIEW MORE PLAN\"', async function() {
    await actionCommon.clickElement(clickNextBtn.viewMorePlansLinkBtn);

});



Then('I see Med + Care Counseling , Med + therapy, Therapy plan', async function() {
    // assert o day

});



When('I click on “Start Therapy ” on Therapy plan', async function() {
    // Write code here that turns the phrase above into concrete actions
    await actionCommon.clickElement(clickNextBtn.startTherapiBtn);
});



Then('I get navigated to Billing Information screen', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Then('I see “Therapy”', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Then('the price is for first month is $23\/week', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Then('the price for month later is $60\/week', async function() {
    // Write code here that turns the phrase above into concrete actions

});



When('I complete check out', async function() {
    await testController.scroll("bottom");
    await actionCommon.typeTextToTextBox(checkOutInformation.inputNameCredit, "Phat Test");
    await testController.switchToIframe(checkOutInformation.iframe1);
    await actionCommon.typeTextToTextBox(checkOutInformation.inputNumberCredit, "4242424242424242");
    await testController.switchToMainWindow();
    await testController.switchToIframe(checkOutInformation.iframe2);
    await actionCommon.typeTextToTextBox(checkOutInformation.inputMMYYCredit, "1221");
    await testController.switchToMainWindow();
    await testController.switchToIframe(checkOutInformation.iframe3);
    await actionCommon.typeTextToTextBox(checkOutInformation.inputCVCCredit, "111");
    await testController.switchToMainWindow();
    await actionCommon.typeTextToTextBox(checkOutInformation.inputZIPCredit, "77098");
    await actionCommon.clickElement(checkOutInformation.submitBtn);
});







When('click “Start Meds + Therapy Plan” on add Medication plan', async function() {
    // Write code here that turns the phrase above into concrete actions
    await actionCommon.clickElement(startTherapit.startMedAndTherapiTodayBtn);
    await actionCommon.clickElement(startTherapit.confirmBtn);
    await actionCommon.clickElement(startTherapit.continuePlanBtn);
});



When('complete shipping information', async function() {
    // Write code here that turns the phrase above into concrete actions
    await actionCommon.typeTextToTextBox(shippingInfor.addressLine1Txt, 'asdsadsadsad');
    await actionCommon.typeTextToTextBox(shippingInfor.addressLine2Txt, 'asdsadsadsad');
    await actionCommon.typeTextToTextBox(shippingInfor.cityTxt, 'adwqeqeqwe');
});



When('complete post checkout question', async function() {
    // Write code here that turns the phrase above into concrete actions

});



When('Choose schedule with Prescriber', async function() {
    // Write code here that turns the phrase above into concrete actions

});



When('schedule later with Therapist', async function() {
    // Write code here that turns the phrase above into concrete actions

});



When('continue to the client portal', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Then('I get navigated to Message box chat', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Then('see only greeting message from Cerebral Care team', async function() {
    // Write code here that turns the phrase above into concrete actions

});



When('I click on “Account”', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Then('I cant see “Edit” on “PHARMACY BENEFITS \/ MEDICATION BILLING”', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Then('cant see “Check my insurance coverage” on INSURANCE INFORMATION', async function() {
    // Write code here that turns the phrase above into concrete actions

});



When('I click on \"Appointments & Tasks\"', async function(string) {
    // Write code here that turns the phrase above into concrete actions

});



When('Click Start on MY OUTSTANDING TASKS', async function() {
    // Write code here that turns the phrase above into concrete actions

});




Then('I can\'t see insurance popup', async function() {
    // Write code here that turns the phrase above into concrete actions

});



When('I open my email that is used to register my Cerebral account', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Then('I receive a Welcome email from Cerebral', async function() {
    // Write code here that turns the phrase above into concrete actions

});



Then('appointment confirmation email', async function() {
    // Write code here that turns the phrase above into concrete actions

});