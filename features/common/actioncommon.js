const { Selector } = require("testcafe");
const { Given, When, Then } = require("cucumber");

class actioncommon {
    async waitUntil(element, time) {
        await testController
            .expect(element.with({ boundTestRun: testController }).visible)
            .ok({ timeout: time });
    }

    async typeTextToTextBox(element, value) {
        await this.waitUntil(element, 180000);
        await testController.typeText(element, value);
    }

    async navigateToUrl(UrlWebsite) {
        await testController.navigateTo(UrlWebsite);
    }

    async clickElement(element) {
        await this.waitUntil(element, 180000);
        await testController.click(element);
    }

    async clickCheckBox(element) {
        await this.waitUntil(element, 180000);
        await testController.click(element);
    }

    generateRandomString(length) {
        const characters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let result = "";
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    }
}
module.exports = new actioncommon();